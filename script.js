const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
const MOVIMENTO = 50
let container = document.querySelector("#container")

for(i=0; i < map.length; i++) {
    for(j=0; j < map[i].length; j++) {
        let div = document.createElement("div")
        div.id = `${i} ${j}`
        div.textContent = map[i][j]
        if(div.textContent === "W"){
            div.classList.add("W")

        }else if(div.textContent === " "){
            div.classList.add("vazio")

        }else if(div.textContent === "S"){
            div.classList.add("inicio")

        }else if(div.textContent === "F"){
            div.classList.add("fim")
        }
        div.innerHTML = ""

        container.appendChild(div)
    }

}

let coluna = 9
let linha = 0 
let linhaxcoluna

document.addEventListener('keydown', (event) => {
    keyName = event.key
    console.log(keyName)    
     
     if(keyName === "ArrowDown"){
 
         let jogador = document.getElementById(`${coluna} ${linha}`)
         let move = document.getElementById(`${coluna+1} ${linha}`)

         if(move.classList.contains("vazio")){
            jogador.classList.remove('inicio');
            move.classList.add('inicio');
            coluna++
            console.log("certo")
         }
        if(move.classList.contains("fim")){
            alert("vitoria")
            window.location.reload();
        }

     }else if(keyName === "ArrowUp"){
        let jogador = document.getElementById(`${coluna} ${linha}`)
        let move = document.getElementById(`${coluna-1} ${linha}`)

         if(move.classList.contains("vazio")){
            jogador.classList.remove('inicio');
            move.classList.add('inicio');
            coluna--
            console.log("certo")
         }
         if(move.classList.contains("fim")){
            alert("vitoria")
            window.location.reload();
        }



     }else if(keyName === "ArrowRight"){
        let jogador = document.getElementById(`${coluna} ${linha}`)
        let move = document.getElementById(`${coluna} ${linha+1}`)

        if(move.classList.contains("vazio")){
            console.log(jogador)
            jogador.classList.remove('inicio');
            jogador.classList.add('vazio');

            move.classList.add('inicio');
            linha++
            console.log("certo")
         }
         if(move.classList.contains("fim")){
            alert("vitoria")
            window.location.reload();
        }




     }else if(keyName === "ArrowLeft"){
        let jogador = document.getElementById(`${coluna} ${linha}`)
        let move = document.getElementById(`${coluna} ${linha-1}`)

        if(move.classList.contains("vazio")){
            jogador.classList.remove('inicio');
            move.classList.add('inicio');
            linha--
            console.log("certo")
         }
         if(move.classList.contains("fim")){
            alert("vitoria")
            window.location.reload();
        }


        
     }
 
 })
 